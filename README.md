# Slim3 Framework REST Api Aivo

[![StyleCI](https://bitbucket.org/hatsem78/api_aivo/src)](https://styleci.io/repos/116841369)
[![Build Status](https://bitbucket.org/hatsem78/api_aivo/src)](https://bitbucket.org/hatsem78/api_aivo/src)

Este es un simple rest-api twitter account.

## Instalación

#### Git Clone

Para obtener la última fuente puedes usar git clone.

`$ git clone https://hatsem78@bitbucket.org/hatsem78/api_aivo.git

#### Composer

La instalación se puede hacer con el uso del compositor. Si aún no tienes un compositor, puedes instalarlo haciendo:

`$ curl -s https://getcomposer.org/installer | php`

Para instalarlo globalmente.

`$ sudo mv composer.phar /usr/local/bin/composer`

```
$ cd /path/to/api_aivo
$ composer update
$ composer install
```

## Configuración base de datos

 - Import `dababase/mysql.sql` en su MySQL Server, y crear las tablas correspondientes.
 - Copiar `.env.example` renombrar por `.env`  
 - En el archivo  .env proporcionar las credenciales correspondientes
 
```
// .env 

# Database
DB_DRIVER=mysql
DB_HOST=localhost
DB_NAME=
DB_USERNAME=
DB_PASSWORD=

```

## Usage

### Api Endpoints

```

$ GET       /api/v1/twitterS          // retorna los ultimos 10 twitter

```



### Responses

Cada endpoint proporciona una respuesta JSON con datos relevantes.

#### $ GET /api/v1/twitters

```
{
    'code': 200,
    'data': [
    
        {
            "created_at": "Mon Jun 25 16:03:38 +0000 2018",
            "text": "esto es un tweet",
            "in_reply": {
                "id": 123465,
                "name": "john doe"
            }
        }
        { ... },
        { ... },
        ...
    ]
}

```
#### RUN SERVER
```
 php -S localhost:9000 -t public public/index.php


```

#### RUN UNITTEST
```
php composer.phar test

```

