<?php

namespace App\Models;

use Slim\Http\Request;
use Slim\Http\Response;
use App\Transformers\TweetsTransformer;

class Tweets extends BaseModel
{

    public function getAllLastTen(){
        $tweets = new Tweets();
        $tweets_transformer = new TweetsTransformer();
        $result = $tweets->join('account',function($join){
            $join->on('account.id','=','tweets.account_id');
            })
            ->select('*')
            ->selectRaw("(SELECT CONCAT('{\"id\":',account.id,',\"name\": \"',`last_name`,' ',`name`,'\"}') 
                            from account AS account where id = tweets.in_reply) as in_replys")
            ->orderBy('created_at','DESC')
            ->limit(10)
            ->get();

        return $tweets_transformer->transformCollection($result);
    }
    public function personas(){
        return $this::where('tipo',1)->get();
    }
}