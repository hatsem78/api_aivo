<?php

namespace App\Transformers;

use App\Models\Tweets;
use Illuminate\Database\Eloquent\Collection;

class TweetsTransformer
{
    /**
     * @param User $user
     *
     * @return array
     */
    public static function transform(Tweets $tweets)
    {
        return [
            'id'         => $tweets->id,
            'text'       => $tweets->text,
            'created_at' => $tweets->created_at,
            'in_reply'   => json_decode($tweets->in_replys)
                                        //'{"id":1,"name":"Perez Juan"}'
                                        //'{"id":1,"name:"Perez Juan"}'
        ];
    }
    /**
     * @param Collection $tweets
     *
     * @return array
     */
    public function transformCollection(Collection $tweets)
    {
        $data = array();

        foreach ($tweets as $tweet) {
            $data[] = TweetsTransformer::transform($tweet);
        }

        return $data;
    }
}
