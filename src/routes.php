<?php

// Routes
use Slim\Http\Request;
use Slim\Http\Response;
use App\Models\Tweets;

// example home route
$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Api-Aivo '/' route");
    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
$tweets = new Tweets();
$app->group('/api', function () use ($app) {
    $app->group('/v1', function () use ($app) {

        /**
         * Route GET /api/v1/tweets
         */
        $app->get('/tweets', function ($request, $response) {
            $tweets = new Tweets();
            $tweet = $tweets->getAllLastTen();
            // logging within the controller
            $this->logger->info($request->getUri() . " route");

            return $response->withJson([
                'data' => $tweet
            ]);
        });

    });
});

