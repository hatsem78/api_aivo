<?php

namespace Tests\Functional;

error_reporting(-1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
class TweetsTest extends BaseTestCase
{
    /**
     * comprueba si devuelve una reuta
    **/
    public function test_tweetsEndpoint()
    {
        $response = $this->runApp('GET', '/');

        $this->assertEquals(200, $response->getStatusCode());
    }


    /**
     * Test prueba que devuelva los ultimos 10 tweets
     */
    public function testtweetsLastTenEndpoint()
    {

        $response = $this->runApp('GET', '/api/v1/tweets');

        $collection = (array) json_decode($response->getBody());
        print_r(count($collection['data']));

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertEquals(10, count($collection['data']));

        $this->assertJson((string) $response->getBody());

    }

}